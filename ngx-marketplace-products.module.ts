import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMarketplaceProductsService } from './ngx-marketplace-products.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    NgxMarketplaceProductsService
  ],
  exports: [
  ]
})
export class NgxMarketplaceProductsModule { }
