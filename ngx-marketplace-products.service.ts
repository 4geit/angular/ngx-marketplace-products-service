import { Inject, Injectable, Optional } from '@angular/core';

import { ITEMS, BASE_PATH } from './variables';

@Injectable()
export class NgxMarketplaceProductsService {

  items = [];
  basePath = '/product';

  constructor(
    @Optional()@Inject(ITEMS) items: Array<string>,
    @Optional()@Inject(BASE_PATH) basePath: string,
  ) {
    if (items) { this.items = items; }
    if (basePath) { this.basePath = basePath; }
  }

  getProductDetail(slug: string) {
    const result = this.items.filter(item => item.slug === slug);
    if (result.length) {
      return result[0];
    }
    throw Error('no product found with this slug.');
  }

}
