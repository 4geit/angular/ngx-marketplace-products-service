import { TestBed, inject } from '@angular/core/testing';

import { NgxMarketplaceProductsService } from './marketplace-products.service';

describe('NgxMarketplaceProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxMarketplaceProductsService]
    });
  });

  it('should be created', inject([NgxMarketplaceProductsService], (service: NgxMarketplaceProductsService) => {
    expect(service).toBeTruthy();
  }));
});
